package test;

import java.util.Set;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

import model.Account;
import model.Bank;
import model.Person;

public class TestBank {

		@Test(expected=AssertionError.class)
		
		public void testFSpending()
		{
			Bank b=new Bank();
			
			b.addPerson("Testat");
			
			for(Person p:b.getPeople())
				b.createSpendingAccount(p, -1);
			
			assert true;
		}
		
		@Test
		public void testTSaving()
		{
			Bank b=new Bank();
			
			b.addPerson("Save");
			
			Person current=null;
			
			for(Person p:b.getPeople())
			{
				current=p;
				b.createSavingAccount(current, 40, 0.2f);
				b.createSavingAccount(current, 32, 0.5f);
				b.createSavingAccount(current, 48, 0.0f);
				b.computeInterest(current);
			}
			
			boolean check=true;
			
			Set<Account> sa=b.getAccounts(current);
			for(Account d:sa)
				if(d.getSum()!=48.0f)
				{
					System.out.println(d.getSum());
					check=false;
				}
			assertTrue(check==true);
		}
		
		@Test(expected=AssertionError.class)
		public void testFWithDraw()
		{
			Bank b=new Bank();
			
			b.addPerson("WithP");
			
			for(Person p:b.getPeople())
			{
				b.createSpendingAccount(p, 50);
				for(Account a:b.getAccounts(p))
					b.withdrawMoney(a, 51);
			}
			assert true;
		}
		
		@Test
		public void testADeposit()
		{
			Bank b=new Bank();
			
			b.addPerson("DepP");
			
			boolean check=true;
			
			for(Person p:b.getPeople())
			{
				b.createSpendingAccount(p, 50);
				
				for(Account a:b.getAccounts(p))
					for(int i=1;i<=4;i++)
						b.depositMoney(a, 100);
				
				for(Account a:b.getAccounts(p))
					if(a.getSum()!=450)
						check=false;
			}
			assertTrue(check==true);
			
		}
		
		@Test(expected=AssertionError.class)
		public void removeFPerson()
		{
			Bank b=new Bank();
			
			Person p=new Person(1,"Removed");
			b.removePerson(p);
			b.addPerson("DepP");
			
			assert true;
		}
}
