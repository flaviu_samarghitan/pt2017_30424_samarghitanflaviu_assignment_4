package presentation;

import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelListener;

public class ViewMain extends JFrame {

	private JPanel contentPane;
	
	private JScrollPane accountPane; 
	private JScrollPane personPane; 
	
	private JTextField txtName;
	private JTextField txtSum;
	
	private JButton btnAddAccount;
	private JButton btnRemoveAccount;
	
	private JButton btnAddPerson;
	private JButton btnRemovePerson;
	
	private JButton btnWithdraw;
	private JButton btnDeposit;
	
	public JRadioButton rdbtnSavingAccount;
	public JRadioButton rdbtnSpendingAccount;
	
	public JTable tableAccounts;
	public JTable tablePeople;
	
	public ButtonGroup g;
	private JTextField txtInterest;
	private JLabel lblSum;
	private JButton btnUpdateAccounts;
	public JTextField txtEName;
	private JLabel lblNewLabel_1;
	private JButton btnSaveChanges;
	private JLabel lblEditSelectedAcount;
	public JTextField txtESum;
	private JLabel lblSum_1;
	public JTextField txtETrans;
	private JLabel lblNrTransactions;
	public JTextField txtEInt;
	private JLabel lblInterest;
	private JLabel lblName;
	private JButton btnSaveAccountChanges;
	
	private MouseListener mt;
	private MouseListener mp;
	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public ViewMain() {
		setTitle("Bank management");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 799, 676);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		tableAccounts = new JTable();
		tableAccounts.setEnabled(false);

		accountPane = new JScrollPane(tableAccounts);
		accountPane.setBounds(23, 23, 345, 189);
		contentPane.add(accountPane);
		
		tablePeople = new JTable();
		tablePeople.setEnabled(false);
		
		personPane = new JScrollPane(tablePeople);
		personPane.setBounds(23, 281, 345, 170);
		contentPane.add(personPane);
		
		
		btnAddAccount = new JButton("Add account");
		btnAddAccount.setBounds(581, 75, 153, 25);
		contentPane.add(btnAddAccount);
		btnAddAccount.setActionCommand("adda");
		
		btnRemoveAccount = new JButton("Remove account");
		btnRemoveAccount.setBounds(581, 113, 153, 25);
		contentPane.add(btnRemoveAccount);
		btnRemoveAccount.setActionCommand("rema");
		
		JLabel lblNewLabel = new JLabel("Name: ");
		lblNewLabel.setBounds(396, 307, 56, 16);
		contentPane.add(lblNewLabel);
		
		txtName = new JTextField();
		txtName.setBounds(477, 304, 116, 22);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		btnAddPerson = new JButton("Add person");
		btnAddPerson.setBounds(617, 303, 131, 25);
		contentPane.add(btnAddPerson);
		btnAddPerson.setActionCommand("addp");
		
		g=new ButtonGroup();
		
		rdbtnSavingAccount = new JRadioButton("Saving account");
		rdbtnSavingAccount.setBounds(396, 31, 139, 25);
		contentPane.add(rdbtnSavingAccount);
		
		rdbtnSavingAccount.setSelected(true);
		g.add(rdbtnSavingAccount);
		
		rdbtnSpendingAccount = new JRadioButton("Spending account");
		rdbtnSpendingAccount.setBounds(396, 75, 139, 25);
		contentPane.add(rdbtnSpendingAccount);
		g.add(rdbtnSpendingAccount);
		
		btnRemovePerson = new JButton("Remove person");
		btnRemovePerson.setBounds(617, 341, 131, 25);
		contentPane.add(btnRemovePerson);
		btnRemovePerson.setActionCommand("remp");
		
		txtSum = new JTextField();
		txtSum.setBounds(386, 169, 116, 22);
		contentPane.add(txtSum);
		txtSum.setColumns(10);
		
		btnWithdraw = new JButton("Withdraw");
		btnWithdraw.setBounds(552, 168, 107, 25);
		contentPane.add(btnWithdraw);
		btnWithdraw.setActionCommand("with");
		
		btnDeposit = new JButton("Deposit");
		btnDeposit.setBounds(552, 206, 107, 25);
		contentPane.add(btnDeposit);
		btnDeposit.setActionCommand("dep");
		
		txtInterest = new JTextField();
		txtInterest.setText("0.1");
		txtInterest.setBounds(543, 32, 116, 22);
		contentPane.add(txtInterest);
		txtInterest.setColumns(10);
		
		lblSum = new JLabel("Sum");
		lblSum.setBounds(420, 138, 56, 16);
		contentPane.add(lblSum);
		
		btnUpdateAccounts = new JButton("Compute interests");
		btnUpdateAccounts.setBounds(593, 426, 166, 25);
		contentPane.add(btnUpdateAccounts);
		btnUpdateAccounts.setActionCommand("ua");
		
		txtEName = new JTextField();
		txtEName.setBounds(196, 490, 116, 22);
		contentPane.add(txtEName);
		txtEName.setColumns(10);
		
		lblNewLabel_1 = new JLabel("Edit selected person:");
		lblNewLabel_1.setBounds(33, 493, 153, 16);
		contentPane.add(lblNewLabel_1);
		
		btnSaveChanges = new JButton("Save person changes");
		btnSaveChanges.setBounds(336, 489, 181, 25);
		contentPane.add(btnSaveChanges);
		btnSaveChanges.setActionCommand("sa");
		
		lblEditSelectedAcount = new JLabel("Edit selected account:");
		lblEditSelectedAcount.setBounds(33, 555, 139, 16);
		contentPane.add(lblEditSelectedAcount);
		
		txtESum = new JTextField();
		txtESum.setBounds(198, 552, 116, 22);
		contentPane.add(txtESum);
		txtESum.setColumns(10);
		
		lblSum_1 = new JLabel("Sum");
		lblSum_1.setBounds(229, 531, 56, 16);
		contentPane.add(lblSum_1);
		
		txtETrans = new JTextField();
		txtETrans.setBounds(336, 552, 116, 22);
		contentPane.add(txtETrans);
		txtETrans.setColumns(10);
		
		lblNrTransactions = new JLabel("Nr. Transactions");
		lblNrTransactions.setBounds(346, 531, 111, 16);
		contentPane.add(lblNrTransactions);
		
		txtEInt = new JTextField();
		txtEInt.setBounds(477, 552, 116, 22);
		contentPane.add(txtEInt);
		txtEInt.setColumns(10);
		
		lblInterest = new JLabel("Interest");
		lblInterest.setBounds(506, 531, 63, 16);
		contentPane.add(lblInterest);
		
		lblName = new JLabel("Name");
		lblName.setBounds(229, 462, 56, 16);
		contentPane.add(lblName);
		
		btnSaveAccountChanges = new JButton("Save account changes");
		btnSaveAccountChanges.setBounds(603, 552, 170, 23);
		contentPane.add(btnSaveAccountChanges);
		btnSaveAccountChanges.setActionCommand("sca");
	}
	public void setBtnListener(ActionListener al)
	{
		btnDeposit.addActionListener(al);
		btnRemovePerson.addActionListener(al);
		btnAddPerson.addActionListener(al);
		btnAddAccount.addActionListener(al);
		btnRemoveAccount.addActionListener(al);
		btnDeposit.addActionListener(al);
		btnWithdraw.addActionListener(al);
		btnUpdateAccounts.addActionListener(al);
		btnSaveChanges.addActionListener(al);
		btnSaveAccountChanges.addActionListener(al);
	}
	public void setTabelListener(TableModelListener al)
	{
		tableAccounts.getModel().addTableModelListener(al);
		tablePeople.getModel().addTableModelListener(al);
	}
	
	public String getSum()
	{
		return txtSum.getText();
	}
	public String getName()
	{
		return txtName.getText();
	}
	
	public String getInterest()
	{
		return txtInterest.getText();
	}
	public void setPeopleTable(JTable t)
	{
		tablePeople=t;
		tablePeople.addMouseListener(mp);
		contentPane.remove(personPane);
		personPane=new JScrollPane(tablePeople);
		personPane.setBounds(23, 281, 345, 170);
		contentPane.add(personPane);
	}
	public void setAccountTable(JTable t)
	{
		tableAccounts=t;
		tableAccounts.addMouseListener(mt);
		contentPane.remove(accountPane);
		accountPane=new JScrollPane(tableAccounts);
		accountPane.setBounds(23, 23, 345, 189);
		contentPane.add(accountPane);
	}
	public int getSelectedPerson()
	{
		int i=tablePeople.getSelectedRow();
		if(i==-1)
			return i;
		return Integer.parseInt((String)tablePeople.getValueAt(i, 0));
		 
	}
	
	public int getSelectedAccount()
	{
		int i=tableAccounts.getSelectedRow();
		if(i==-1)
			return i;
		return Integer.parseInt((String)tableAccounts.getValueAt(i, 0));
		
	}
	public void setAcountListener(MouseListener mt)
	{
		this.mt=mt;
		tableAccounts.addMouseListener(mt);
	}
	public void setPersonListener(MouseListener mp)
	{
		this.mp=mp;
		tablePeople.addMouseListener(mp);
	}
}
