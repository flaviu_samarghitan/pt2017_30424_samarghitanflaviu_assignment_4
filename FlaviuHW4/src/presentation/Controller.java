package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JOptionPane;
import javax.swing.JTable;

import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;

public class Controller {

	private ViewMain v;
	private Bank b;
	
	public Controller()
	{
		
		this.v=new ViewMain();
		v.setVisible(true);
		b=new Bank();
		b.loadBank();
		
		JTable t=createPersonTable(b.getPeople());
		v.setPeopleTable(t);
		
		Set<Account> allA=new HashSet<Account>();
		for(Person i: b.getPeople())
			if(b.getAccounts(i)!=null)
				allA.addAll(b.getAccounts(i));
		JTable ta=createAccountTable(allA);
		v.setAccountTable(ta);
		
		class BtnActionListener implements ActionListener 
		{

			public void actionPerformed(ActionEvent e) 
			{
				if(e.getActionCommand()=="addp")
				{
					String name=null;
					try
					{
						name=v.getName();
						if(name.equals(""))
							throw new Exception();
					}
					catch(Exception ed)
					{
						JOptionPane.showMessageDialog(null,"The name must be a string");
					}
					b.addPerson(name);
					JTable t=createPersonTable(b.getPeople());
					v.setPeopleTable(t);
					b.saveBank();
				}
				if(e.getActionCommand()=="adda")
				{
					float sum=0;
					try
					{
						sum=Float.parseFloat(v.getSum());
					}
					catch(Exception ed)
					{
						JOptionPane.showMessageDialog(null,"The sum of money must be a number");
					}
					if(sum<0)
						JOptionPane.showMessageDialog(null,"Enter a positive sum of money!");
					else
					{
						Person p=findPerson(v.getSelectedPerson());
						
						if(v.rdbtnSavingAccount.isSelected()==true)
						{
							float interest=Float.parseFloat(v.getInterest());
							b.createSavingAccount(p, sum, interest);
						}
						if(v.rdbtnSpendingAccount.isSelected()==true)
							b.createSpendingAccount(p, sum);
						
						Set<Account> allA=new HashSet<Account>();
						for(Person i: b.getPeople())
							if(b.getAccounts(i)!=null)
								allA.addAll(b.getAccounts(i));
						JTable t=createAccountTable(allA);
						v.setAccountTable(t);
						b.saveBank();
					}
					
				}
				
				if(e.getActionCommand()=="remp")
				{
					Person rem=findPerson(v.getSelectedPerson());
					if(rem==null)
						JOptionPane.showMessageDialog(null,"Selected a person in order to remove it!");
					{
						b.removePerson(rem);
						JTable t=createPersonTable(b.getPeople());
						v.setPeopleTable(t);
						
						Set<Account> allA=new HashSet<Account>();
						for(Person i: b.getPeople())
							if(b.getAccounts(i)!=null)
								allA.addAll(b.getAccounts(i));
						JTable ta=createAccountTable(allA);
						v.setAccountTable(ta);
						b.saveBank();
					}
				}
				if(e.getActionCommand()=="rema")
				{
					int aid=v.getSelectedAccount();
					if(aid==-1)
						JOptionPane.showMessageDialog(null,"Selected an account in order to remove it!");
					else
					{
						Set<Account> allA=new HashSet<Account>();
						
						for(Person i: b.getPeople())
						{
							Iterator<Account> it=b.getAccounts(i).iterator();
							while(it.hasNext())
							{
								Account a=it.next();
		
								if(a.getId()==aid)
									b.removeAccount(i,a);
							}
							if(b.getAccounts(i)!=null)
								allA.addAll(b.getAccounts(i));
						}
						JTable ta=createAccountTable(allA);
						v.setAccountTable(ta);
						b.saveBank();
					}
				}
				if(e.getActionCommand()=="with")
				{
					float sum=0;
					try
					{
						sum=Float.parseFloat(v.getSum());
					}
					catch(Exception ed)
					{
						JOptionPane.showMessageDialog(null,"The sum of money must be a number");
					}
					
					int aid=v.getSelectedAccount();
					if(aid==-1)
						JOptionPane.showMessageDialog(null,"Selected an account in order to withdraw from it!");
					else
					{
					
						Set<Account> allA=new HashSet<Account>();
						
						for(Person i: b.getPeople())
						{
							for(Account a:b.getAccounts(i))
								if(a.getId()==aid)
								{
									if(a.getSum()<sum)
										JOptionPane.showMessageDialog(null,"The withdrawn sum must be smaller than the amount in the account!");
									else
										b.withdrawMoney(a, sum);
								}
							if(b.getAccounts(i)!=null)
								allA.addAll(b.getAccounts(i));
						}
						JTable ta=createAccountTable(allA);
						v.setAccountTable(ta);
						b.saveBank();
					}
				}
				if(e.getActionCommand()=="dep")
				{
					float sum=0;
					try
					{
						sum=Float.parseFloat(v.getSum());
					}
					catch(Exception ed)
					{
						JOptionPane.showMessageDialog(null,"The sum of money must be a number");
					}
					int aid=v.getSelectedAccount();
					if(aid==-1)
						JOptionPane.showMessageDialog(null,"Selected an account in order to deposit in it!");
					else
					{
						Set<Account> allA=new HashSet<Account>();
					
						for(Person i: b.getPeople())
						{
							for(Account a:b.getAccounts(i))
								if(a.getId()==aid)
								b.depositMoney(a, sum);
							if(b.getAccounts(i)!=null)
								allA.addAll(b.getAccounts(i));
						}
						
						JTable ta=createAccountTable(allA);
						v.setAccountTable(ta);
						b.saveBank();
					}
				}
				
				if(e.getActionCommand()=="ua")
				{
					Set<Account> allA=new HashSet<Account>();
					
					for(Person i: b.getPeople())
					{
						b.computeInterest(i);
						if(b.getAccounts(i)!=null)
							allA.addAll(b.getAccounts(i));
					}
					JTable ta=createAccountTable(allA);
					v.setAccountTable(ta);
					b.saveBank();
				}
				
				if(e.getActionCommand()=="sa")
				{
					int idP=v.getSelectedPerson();
					
					if(idP!=-1)
					{
						Person p=findPerson(idP);
						p.setName(v.txtEName.getText());
					}
					JTable t=createPersonTable(b.getPeople());
					v.setPeopleTable(t);

					b.saveBank();
					
				}
				if(e.getActionCommand()=="sca")
				{
					int idA=v.getSelectedAccount();
					if(idA!=-1)
					{
						for(Person c:b.getPeople())
							if(b.getAccounts(c)!=null)
							for(Account a:b.getAccounts(c))
								if(a.getId()==idA)
								{
									a.setNrTransactions(Integer.parseInt(v.txtETrans.getText()));
									a.setSum(Float.parseFloat(v.txtESum.getText()));
									if(a.getClass().getSimpleName()=="SavingAccount")
										((SavingAccount)a).setInterest(Float.parseFloat(v.txtEInt.getText()));
								}
						Set<Account> allA=new HashSet<Account>();
						for(Person i: b.getPeople())
							if(b.getAccounts(i)!=null)
								allA.addAll(b.getAccounts(i));
						JTable ta=createAccountTable(allA);
						v.setAccountTable(ta);
					}
				}
				
			}
		}
		BtnActionListener al=new BtnActionListener();
		v.setBtnListener(al);
		
		class MouseTableAListener implements MouseListener
		{	
			public void mouseClicked(java.awt.event.MouseEvent evt)
			{
				int row = v.tableAccounts.rowAtPoint(evt.getPoint());
				if (row >= 0) 
				{
					int id=Integer.parseInt((String)v.tableAccounts.getValueAt(row,0));
					float sum=Float.parseFloat((String)v.tableAccounts.getValueAt(row,1));
					String t=(String)v.tableAccounts.getValueAt(row,2);
					float interest=Float.parseFloat((String)v.tableAccounts.getValueAt(row,3));
					int nrTransactions=Integer.parseInt((String)v.tableAccounts.getValueAt(row, 4));
					
					v.txtEInt.setText(Float.toString(interest));
					v.txtESum.setText(Float.toString(sum));
					v.txtETrans.setText(Integer.toString(nrTransactions));
					
				}

	        }

			@Override
			public void mouseEntered(MouseEvent e) {}

			@Override
			public void mouseExited(MouseEvent e) {}

			@Override
			public void mousePressed(MouseEvent e) {}

			@Override
			public void mouseReleased(MouseEvent e) {}

		}
		class MouseTablePListener implements MouseListener
		{	
			public void mouseClicked(java.awt.event.MouseEvent evt)
			{
				int row = v.tablePeople.rowAtPoint(evt.getPoint());
				if (row >= 0) 
				{
					int id=Integer.parseInt((String)v.tablePeople.getValueAt(row,0));
					String name=(String)v.tablePeople.getValueAt(row,1);
					
					v.txtEName.setText(name);	
					
					Set<Account> show=new HashSet<Account>();
					
					Person p=findPerson(id);
					Set<Account> sa=b.getAccounts(p);
					if(sa!=null)
						for(Account a:sa)
								show.add(a);
					JTable ca=createAccountTable(show);
					v.setAccountTable(ca);
					
				}

	        }

			@Override
			public void mouseEntered(MouseEvent e) {}

			@Override
			public void mouseExited(MouseEvent e) {}

			@Override
			public void mousePressed(MouseEvent e) {}

			@Override
			public void mouseReleased(MouseEvent e) {}

		}
		v.setAcountListener(new MouseTableAListener());
		v.setPersonListener(new MouseTablePListener());
	}	
	private JTable createPersonTable(Set<Person> ac)
	{

		String[][] t=new String[ac.size()][2];
		String[] firstRow=new String[2];

		firstRow[0]="ID";
		firstRow[1]="Name";
			
		int i=0;
		for(Person p:ac)
		{
			t[i][0]=Integer.toString(p.getId());
			t[i][1]=p.getName();
			i++;
		}
		return new JTable(t,firstRow);
	}
	
	private Person findPerson(int id)
	{
		for(Person q:b.getPeople())
			if(q.getId()==id)
				return q;
		return null;
	}
	private JTable createAccountTable(Set<Account> ac)
	{

		String[][] t=new String[ac.size()][5];
		String[] firstRow=new String[5];

		firstRow[0]="ID";
		firstRow[1]="Sum";
		firstRow[2]="Type";
		firstRow[3]="Interest";
		firstRow[4]="Transactions";
		
		int i=0;
		for(Account a:ac)
		{
			t[i][0]=Integer.toString(a.getId());
			t[i][1]=Float.toString(a.getSum());
			
			String s=a.getClass().getSimpleName();
			if(s.equals("SavingAccount"))
			{
				t[i][2]="Saving";
				t[i][3]=Float.toString(((SavingAccount)a).getInterest());
			}
			if(s.equals("SpendingAccount"))
			{
				t[i][2]="Spending";
				t[i][3]="0";
			}
			t[i][4]=Integer.toString(a.getNrTransactions());
			
			i++;
		}
		return new JTable(t,firstRow);
	}
	
	public static void main(String[] args)
	{
		Controller c=new Controller();
	}
}