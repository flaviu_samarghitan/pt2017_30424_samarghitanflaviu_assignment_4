package model;

public interface BankProc {
	
		void addPerson(String name);
		
		void removePerson(Person p);
	
		void removeAccount(Person p,Account a);
		
		void createSavingAccount(Person p,float sum, float interest);
		
		void createSpendingAccount(Person p,float sum);
		
		void withdrawMoney(Account a, float sum);
		
		void depositMoney(Account a, float sum);
		
}
