package model;

public class SpendingAccount extends Account {
	
	public SpendingAccount(int id,float sum) 
	{
		super(id, sum);
	}
	
	public void deposit(float sum) 
	{
		setChanged();
		this.sum=this.sum+sum;
		incNrTransactions();
	}
	
	public void withdraw(float sum) 
	{
		setChanged();
		this.sum=this.sum-sum;
		incNrTransactions();
	}


}
