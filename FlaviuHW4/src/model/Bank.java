package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Bank implements BankProc,Serializable{

	private static final long serialVersionUID = -4508779772880005450L;
	int lastP=1,lastA=1;
	private Map<Person,Set<Account>> accounts;
	
	public Bank()
	{
		accounts=new HashMap<Person,Set<Account>>();
	}
	
	public void addPerson(String name)
	{
		assert wellFormed() : "Bank is not corectly formed!";
		
		Person p=new Person(lastP,name);
		lastP++;
		
		int size=accounts.size();
		
		accounts.put(p, null);
		
		assert size+1==accounts.size() : "Person was not added";
		assert wellFormed() : "Bank is not corectly formed!";
	}
	
	public void removePerson(Person p)
	{
		assert wellFormed() : "Bank is not corectly formed!";
		assert accounts.containsKey(p): "Cant remove a person that doesnt belong to the bank!"; 
		
		int size=accounts.size();
		accounts.remove(p);
		
		assert size==accounts.size()+1 : "Person was not removed";
		assert wellFormed() : "Bank is not corectly formed!";
	}

	public void removeAccount(Person p,Account a)
	{
		assert wellFormed() : "Bank is not corectly formed!";
		assert accounts.containsKey(p): "Cant remove an account of a  person that doesnt belong to the bank!"; 
		
		Set<Account> sa=accounts.get(p);
		
		int size=sa.size();
		assert sa.contains(a) : "Account does not belong to the person who attempts to remove it";
		sa.remove(a);
		
		assert size==sa.size()+1:" Deletion of account failed";
		assert wellFormed() : "Bank is not corectly formed!";
	}
	
	public void computeInterest(Person i)
	{
		for(Account a:this.getAccounts(i))
			if(a.getClass().getSimpleName().equals("SavingAccount"))
			{
				((SavingAccount)a).computeInterest();
				a.notifyObservers();
			}
	}
	
	public void createSavingAccount(Person p,float sum, float interest)
	{
		assert wellFormed() : "Bank is not corectly formed!";
		assert sum>0: "The sum can not be negative!";
		assert interest>=0 && interest<=1 :"The interest must be a floating point number between 0 and 1 !";
		assert accounts.containsKey(p): "The person must belong to the bank in order to create an account !";
	
		SavingAccount sa=new SavingAccount(lastA,sum,interest);
		sa.addObserver(p);
		
		lastA++;
		Set<Account> a=accounts.get(p);
		if(a==null)
			a=new HashSet<Account>();
		
		int size=a.size();
		
		a.add(sa);
		accounts.put(p, a);

		assert size+1==a.size(): "Acount was not created succesfully";
		assert wellFormed() : "Bank is not corectly formed!";
	}
	
	public void createSpendingAccount(Person p,float sum)
	{
		assert wellFormed() : "Bank is not corectly formed!";
		assert sum>0: "The sum can not be negative!";
		assert accounts.containsKey(p): "The person must belong to the bank in order to create an account !";
		
		SpendingAccount sa=new SpendingAccount(lastA,sum);
		sa.addObserver(p);
		
		lastA++;
		Set<Account> a=accounts.get(p);
		if(a==null)
			a=new HashSet<Account>();
		
		int size=a.size();
		
		a.add(sa);
		accounts.put(p, a);
		
	
		assert size+1==a.size(): "Acount was not created succesfully";
		assert wellFormed() : "Bank is not corectly formed!";
	}
	
	public void withdrawMoney(Account a, float sum)
	{
		assert wellFormed() : "Bank is not corectly formed!";
		assert sum>0 :"The sum can not be negative!";
		assert containsAccount(a) :"Account does not belong to the bank!";
		
		assert sum<=a.getSum():"Not enough money to be withdrawn!";
		
		float initialSum=a.getSum();
		
		a.withdraw(sum);
		a.notifyObservers();
		
		assert a.getSum()==initialSum-sum : "The operation was not processed corectly!";
		assert wellFormed() : "Bank is not corectly formed!";
	}
	
	public void depositMoney( Account a, float sum)
	{
		assert wellFormed() : "Bank is not corectly formed!";
		assert sum>0 :"The sum can not be negative!";
		assert containsAccount(a) :"Account does not belong to the bank!";
		
		float initialSum=a.getSum();
		
		a.deposit(sum);
		a.notifyObservers();
		
		assert a.getSum()==initialSum+sum : "The operation was not performed corectly!";
		assert wellFormed() : "Bank is not corectly formed!";
	}
	
	public Set<Person> getPeople()
	{
		return accounts.keySet();
	}
	
	public Set<Account> getAccounts(Person p)
	{
		return accounts.get(p);
	}
	
	public void saveBank()
	{
		try {
		
	         FileOutputStream fileOut =new FileOutputStream("bank.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeInt(lastP);
	         out.writeInt(lastA);
	         out.writeObject(accounts);
	         out.close();
	         fileOut.close();
	        }catch(IOException i) {
	         i.printStackTrace();
	        }
	}
	
	public void loadBank()
	{
		try {
	         FileInputStream fileIn = new FileInputStream("bank.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         lastP=in.readInt();
	         lastA= in.readInt();
	         accounts = (Map<Person,Set<Account>>) in.readObject();
	         in.close();
	         fileIn.close();
	      }catch(IOException | ClassNotFoundException i) {
	         i.printStackTrace();
	      }
	}
	
	public boolean containsAccount(Account a)
	{
		for(Person p: this.getPeople())
		{
			Set<Account> c=this.getAccounts(p);
			if(c!=null)
				for(Account ac: c)
					if(ac.getId()==a.getId())
						return true;
		}
		return false;
	}
	public boolean wellFormed()
	{
		if(lastP <1 || lastA<1)
			return false;
		
		Iterator<Map.Entry<Person,Set<Account>>> it = accounts.entrySet().iterator();
	    while (it.hasNext()) 
	    {
	        Map.Entry pair = (Map.Entry)it.next();
	        Person p=(Person)pair.getKey();
	        
	        if(p.getId()<=0 || p.getName().length()<=1)
	        	return false;
	        
	        HashMap<Integer,Boolean> check=new HashMap<Integer,Boolean>();
	        
	        Set<Account>accs=(Set<Account>)pair.getValue() ;
	        if(accs!=null)
		        for(Account a : accs)
		        {
		        	if(a.getId()<=0 || a.getSum()<0 || a.getNrTransactions()<0)
		        		return false;
		        	if(check.get(a.getId())==null)
		        		check.put(a.getId(), true);
		        	else
		        		return false;
		        }
	    }
		return true;
	}
}
