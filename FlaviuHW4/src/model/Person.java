package model;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Person  implements Observer, Serializable{
	
	private transient static final long serialVersionUID = -4455746444837627083L;
	private int id;
	private String name;
	
	public Person(int id, String name)
	{
		this.id=id;
		this.name=name;
	}
	@Override
	public int hashCode() {
		int result = 1;
		result = 17 * result + id;
		result = 17 * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public void update(Observable o,Object sub)
	{
		System.out.println(name+" has been notified that his account with id "+((Account)o).getId()+" has undergone changes! \n");
	}
}

