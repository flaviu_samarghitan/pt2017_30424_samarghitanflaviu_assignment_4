package model;

import java.io.Serializable;
import java.util.Observable;

public abstract class Account extends Observable implements Serializable{
	
	private static final long serialVersionUID = -3966591098093364179L;
	protected int id;
	protected float sum;
	protected int nrTransactions;
	
	public Account(int id, float sum)
	{
		this.id=id;
		this.sum=sum;
		nrTransactions=0;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getSum() {
		return sum;
	}
	public int getNrTransactions() {
		return nrTransactions;
	}
	public void incNrTransactions() {
		this.nrTransactions++;
	}
	
	public void setSum(float sum)
	{
		this.sum=sum;
	}
	
	public void setNrTransactions(int nr)
	{
		nrTransactions=nr;
	}
	
	public abstract void withdraw(float sum);
	public abstract void deposit(float sum); 

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	

}
