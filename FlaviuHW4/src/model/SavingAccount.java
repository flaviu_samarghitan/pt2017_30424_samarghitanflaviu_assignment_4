package model;

public class SavingAccount extends Account{
	
	private float interest;
	private boolean alloW;
	
	public SavingAccount(int id,float sum,float interest)
	{
		super(id,sum);
		this.interest=interest;
		if(sum>0)
			this.alloW=true;
	}
	
	public void deposit(float sum) 
	{
		this.sum=this.sum+sum;
		setChanged();
		incNrTransactions();
		alloW=true;
	}
	
	public void withdraw(float sum) 
	{
		if(alloW==true)
		{
			this.sum=0;
			setChanged();
			alloW=false;
			incNrTransactions();
		}
		else
			throw new IllegalArgumentException();
	}
	public void computeInterest()
	{
		setChanged();
		sum=sum+interest*sum;
	}
	public void setInterest(float in)
	{
		interest=in;
	}
	public float getInterest()
	{
		return interest;
	}
}
